package challenge

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.HttpEntity
import akka.http.scaladsl.model.MediaTypes.`application/json`
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.headers.Cookie
import akka.http.scaladsl.testkit.ScalatestRouteTest
import challenge.Models._
import org.scalatest.{Matchers, OptionValues, WordSpec}
import spray.json._

class ChallengeTest extends WordSpec with Matchers with OptionValues with ScalatestRouteTest with SprayJsonSupport with DefaultJsonProtocol {

  /*
   $sbt test

   basic tests
  */

  val rest = new Rest(new Service(new Repository))

  val userToken1 = rest.TokenService.encodeToken(UserToken(1))
  val userToken2 = rest.TokenService.encodeToken(UserToken(2))

  val messageId = 1
  val testMessage = MessageView(messageId, 1, "message")

  "web part" should {

    "create post" in {
      Post("/create", HttpEntity(`application/json`, testMessage.text)) ~> Cookie("token" → userToken1) ~> rest.route ~> check {
        status shouldBe OK
        responseAs[MessageView] shouldBe testMessage
      }
    }

    "show post" in {
      Get("/show") ~> Cookie("token" → userToken1) ~> rest.route ~> check {
        status shouldBe OK
        responseAs[Seq[MessageView]] shouldBe Seq(testMessage)
      }
    }

    "modify post" in {
      Post(s"/modify?messageId=$messageId", HttpEntity(`application/json`, "update")) ~> Cookie("token" → userToken1) ~> rest.route ~> check {
        status shouldBe OK
        responseAs[MessageView] shouldBe testMessage.copy(text = "update")
      }
    }

    "cannot modify another user's posts" in {
      Post(s"/modify?messageId=$messageId", HttpEntity(`application/json`, "update")) ~> Cookie("token" → userToken2) ~> rest.route ~> check {
        rejection should matchPattern { case ErrorsRejection(errors) if errors.contains("you can only modify your posts") => }
      }
    }

    "cannot delete another user's posts" in {
      Delete(s"/delete?messageId=$messageId") ~> Cookie("token" → userToken2) ~> rest.route ~> check {
        rejection should matchPattern { case ErrorsRejection(errors) if errors.contains("you can only delete your posts") => }
      }
    }

    "delete post" in {
      Delete(s"/delete?messageId=$messageId") ~> Cookie("token" → userToken1) ~> rest.route ~> check {
        status shouldBe OK
      }
    }
  }
}