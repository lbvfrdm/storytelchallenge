package challenge

import akka.http.scaladsl.server.Rejection
import spray.json.DefaultJsonProtocol._

object Models {

  case class MessageDTO(id: Option[Long] = None, userId: Long, text: String, isDelete: Boolean = false)
  implicit val messageDTOFormat = jsonFormat4(MessageDTO)

  case class MessageView(id: Long, userId: Long, text: String)
  implicit val messageViewFormat = jsonFormat3(MessageView)

  case class UserToken(val id: Long) extends AnyVal
  implicit val userTokenFormat = jsonFormat1(UserToken)

  case class ErrorsRejection(errors: Set[String]) extends Rejection {
    def this(msg: String) =  this(Set(msg))
  }
  implicit val errorsRejectionFormat = jsonFormat1(ErrorsRejection)

  implicit class OptsMessageDTO(messageDTO: MessageDTO) {
    def toView: MessageView = {
      MessageView(messageDTO.id.get, messageDTO.userId, messageDTO.text)
    }
  }
}
