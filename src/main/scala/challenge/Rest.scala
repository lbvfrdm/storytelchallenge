package challenge

import java.util.UUID
import java.util.concurrent.atomic.AtomicLong
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.DateTime
import akka.http.scaladsl.model.StatusCodes.{MethodNotAllowed, OK}
import akka.http.scaladsl.model.headers.{Allow, HttpCookie, HttpCookiePair}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import challenge.Models.{ErrorsRejection, UserToken}
import com.wix.accord.dsl.{between, validator, _}
import com.wix.accord.{Descriptions, Result, Validator, Failure => WFailure, Success => WSuccess, validate => validateObj}
import monix.execution.Scheduler.Implicits.global
import pdi.jwt.{Jwt, JwtAlgorithm, JwtClaim}
import spray.json.DefaultJsonProtocol._
import spray.json.{JsonReader, JsonWriter, _}
import scala.util.{Failure, Success, Try}

class Rest(service: Service) extends CustomDirectives with SprayJsonSupport {

  val (messageMinSize,messageMaxSize) = (1, 200)

  implicit val messageValidator = validator[String] { message =>
    message.length as s"message size must be from $messageMinSize to $messageMaxSize characters" should between(messageMinSize, messageMaxSize)
  }

  val route =

    checkJWTToken { userToken =>

      (path("show") & get) {
        val result = service.get.runToFuture
        onSuccess(result) { messages => complete(messages.map(_.toView)) }
      } ~
      (path("create") & post & entity(as[String])) { message =>
        customValidate(message)(implicitly) {

          val result = service.create(userToken, message).runToFuture
          onSuccess(result) { createdMessage =>
            complete(createdMessage.toView)
          }
        }
      } ~
      (path("modify") & post & parameters('messageId.as[Long]) & entity(as[String])) { (messageId, message) =>
        customValidate(message)(implicitly) {

          val result = service.modify(userToken, messageId, message).runToFuture
          onSuccess(result) { either =>
            either match {
              case Right(updatedMessage) => complete(updatedMessage.toView)
              case Left(error) => reject(new ErrorsRejection(error))
            }
          }
        }
      } ~
      (path("delete") & delete & parameters('messageId.as[Long])) { messageId =>

          val result = service.delete(userToken, messageId).runToFuture
          onSuccess(result) { either =>
            either match {
              case Right(_)=> complete(OK)
              case Left(error) => reject(new ErrorsRejection(error))
            }
          }
        }
    } ~
    path("logout") {
        deleteCookie(cookieName, path= "/") {
          complete(OK)
        }
    }

  implicit def  myExceptionHandler = ExceptionHandler {
    case ex: RuntimeException =>
      extractUri { uri =>
        complete((OK, new ErrorsRejection("An error occurred, please try again later")))
      }
  }

  implicit def rejectionHandler: RejectionHandler = RejectionHandler.newBuilder()
    .handle({
      case errorsRejection : ErrorsRejection  => {
        complete((OK, errorsRejection))
      }
    })
    .handleAll[MethodRejection] { rejections =>
    val methods = rejections map (_.supported)
    lazy val names = methods map (_.name) mkString ", "

    respondWithHeader(Allow(methods)) {
      options {
        complete(s"Supported methods : $names.")
      } ~
        complete(MethodNotAllowed,
          s"HTTP method not allowed, supported methods: $names!")
    }
  }.result()
}

trait CustomDirectives {

  val userIds = new AtomicLong(0)
  val cookieName = "token"

  private def createCookie(cookieName: String, stringToken: String, expiresFromNow: (DateTime => DateTime) = (now) => now.copy(year = now.year + 1)) = {
    HttpCookie(cookieName, stringToken, path = Some("/"), expires = Some(expiresFromNow(DateTime.now)))
  }

  def checkJWTToken: Directive1[UserToken] = {

    import TokenService._

    def createToken(): (UserToken, String) = {
      val userToken = new UserToken(userIds.incrementAndGet())
      val stringToken = encodeToken(userToken)
      (userToken, stringToken)
    }

    optionalCookie(cookieName).flatMap({

      case Some(nameCookie: HttpCookiePair) => {
        val token: String = nameCookie.value
        val tryUserToken = decodeToken[UserToken](token)

        tryUserToken match {
          case Success(userToken) => provide(userToken)
          case Failure(ex) => {
            val (userToken, stringToken) = createToken()
            setCookie(createCookie(cookieName, stringToken)).tflatMap(x => {
              provide(userToken)
            })
          }
        }
      }
      case None => {
        val (userToken, stringToken) = createToken()
        setCookie(createCookie(cookieName, stringToken)).tflatMap(x => {
          provide(userToken)
        })
      }
    })
  }

  def customValidate[T: Validator](value: T): Directive0 = {
    Directive {
      inner => {
        val validateResult: Result = validateObj(value)
        validateResult match {
          case WSuccess => inner(())
          case WFailure(violations) => {
            val result: Set[String] = violations.flatMap(_.path).map(Descriptions.render(_))
            reject(ErrorsRejection(result))
          }
        }
      }
    }
  }

  object TokenService {

    private val secretKey = UUID.randomUUID().toString
    private val algorithm = JwtAlgorithm.HS256
    private val seqOfAlgorithm = Seq(JwtAlgorithm.HS256)

    private val expiresTime = 60*60*24*30 //seconds - 1 month

    def encodeToken[T](data: T)(implicit jw: JsonWriter[T]): String = {
      val strJson = data.toJson.toString()
      val claim = JwtClaim(strJson).expiresIn(expiresTime)
      Jwt.encode(claim, secretKey, algorithm)
    }

    def decodeToken[T](token: String)(implicit jw: JsonReader[T]): Try[T] = {

      val decode: Try[String] = Jwt.decodeRaw(token, secretKey, seqOfAlgorithm)

      decode match {
        case Success(v) => Try(v.parseJson.convertTo[T])
        case Failure(ex) => Failure(ex)
      }
    }
  }
}