package challenge

import challenge.Models.MessageDTO
import monix.eval.Task

trait ProfileAndDB {
  val profile: slick.jdbc.JdbcProfile
  import profile.api._
  val database: Database
}

trait H2DB extends ProfileAndDB {
  override val profile = slick.jdbc.H2Profile
  import profile.api._
  override val database = Database.forURL("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1", driver = "org.h2.Driver", keepAliveConnection = true)
}

trait MessageRepository extends MessageAccess {
  this: ProfileAndDB =>

  import profile.api._

  def createScheme()= {
    import monix.execution.Scheduler.Implicits.global
    run(query.schema.create).runSyncUnsafe()
  }

  def getAll: Task[Seq[MessageDTO]] = run(query.filterNot(_.isDelete).result)
  def getById(messageId: Long): Task[Option[MessageDTO]] = run(query.filter(_.id === messageId).result.headOption)

  def save(message: MessageDTO): Task[MessageDTO] = run(queryAI += message)

  def update(messageDTO: MessageDTO): Task[Boolean] = run { update(_.text, messageDTO.text, _.id === messageDTO.id.get,
                                                                                            _.userId === messageDTO.userId,
                                                                                            _.isDelete === false)
                                                          }.map(_ != 0)

  def delete(userId: Long, messageId: Long): Task[Boolean] = run { update(_.isDelete, true, _.id === messageId,
                                                                                            _.userId === userId,
                                                                                            _.isDelete === false)
                                                                 }.map(_ != 0)

  private def run[T](dbio: => DBIO[T])={
    Task.fromFuture(database.run(dbio))
  }

  private def update[T <: Rep[Boolean], R, K](updateFields: (MessageTable => R), updateValue: K, filters: (MessageTable => T)*)(implicit shape: Shape[_ <: FlatShapeLevel, R, K, _]) = {

    val updateQuery = filtered(filters, _ && _)
      .map(updateFields)
      .update(updateValue)

    updateQuery
  }

  private def filtered[T <: Rep[Boolean]](filters: Seq[(MessageTable => T)], func: (Rep[Boolean], Rep[Boolean]) => Rep[Boolean]) = {

    filters match {
      case head +: tail => {
        val filterExpression = (value: MessageTable) => tail.foldLeft[Rep[Boolean]](head(value))((x, y) => func(x, y(value)))
        val filteredQuery = query.filter(filterExpression)
        filteredQuery
      }
      case Nil => query
    }
  }
}

trait MessageAccess {
  this: ProfileAndDB =>

  import profile.api._

  class MessageTable(tag: Tag) extends Table[MessageDTO](tag, "messages") {
    val id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    val userId = column[Long]("user_id")
    val text = column[String]("text")
    val isDelete = column[Boolean]("is_delete")
    def * = (id.?, userId, text, isDelete).mapTo[MessageDTO]
  }

  lazy val query = TableQuery[MessageTable]
  lazy val queryAI = (query returning query.map(_.id)).into((dto, id) => dto.copy(id = Some(id)))
}

class Repository extends MessageRepository with H2DB {
  createScheme()
}