package challenge

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import scala.io.StdIn

object App {

  /*
    $sbt run
   */

  def main(args: Array[String]): Unit = {

    //web
    implicit val system = ActorSystem("storytel-rest")
    implicit val materializer = ActorMaterializer()
    implicit val executionContext = system.dispatcher

    val host = "0.0.0.0"
    val port = 8080

    val rest = new Rest(new Service(new Repository))

    import rest._
    val bindingFuture = Http().bindAndHandle(route, host, port)

    println(s"Server online\nPress RETURN to stop...")
    StdIn.readLine()
    bindingFuture
      .flatMap(_.unbind())
      .onComplete(_ => system.terminate())
  }
}
