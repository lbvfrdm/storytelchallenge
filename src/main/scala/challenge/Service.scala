package challenge

import challenge.Models.{MessageDTO, UserToken}
import monix.eval.Task

class Service(repository: Repository) {

  def get: Task[Seq[MessageDTO]] = repository.getAll

  def create(token: UserToken, message: String): Task[MessageDTO] = repository.save(MessageDTO(userId = token.id, text = message))

  def modify(token: UserToken, messageId: Long, message: String): Task[Either[String, MessageDTO]] = {
    for {
      load <- repository.getById(messageId)
      result  <- load match {
        case Some(messageDTO) if messageDTO.userId != token.id => Task.now(Left("you can only modify your posts"))
        case _ => {
          val updateMessage = MessageDTO(Some(messageId), token.id, message)
          repository.update(updateMessage).map {
            case true => Right(updateMessage)
            case false => Left("message was deleted or does not exist")
          }
        }
      }
    } yield result
  }

  def delete(token: UserToken, messageId: Long): Task[Either[String, Unit]] = {
    for {
      load <- repository.getById(messageId)
      result <- load match {
        case Some(messageDTO) if messageDTO.userId != token.id => Task.now(Left("you can only delete your posts"))
        case _ => {
          repository.delete(token.id, messageId).map {
            case true => Right(())
            case false => Left("message was already deleted or does not exist")
          }
        }
      }
    } yield result
  }
}

