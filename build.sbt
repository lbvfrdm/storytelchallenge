
name := "Storytel"

version := "0.1"

scalaVersion := "2.12.8"

scalacOptions ++= Seq(
  "-Ypartial-unification",
  "-Xfatal-warnings",
  "-unchecked",
  "-feature",
  "-language:existentials",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-language:postfixOps",
  "-deprecation",
  "-encoding", "utf8"
)

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http"   % "10.1.7",
  "com.typesafe.akka" %% "akka-stream" % "2.5.19",
  "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.7",
  "com.typesafe.akka" %% "akka-testkit" % "2.5.19" % "test",
  "com.typesafe.akka" %% "akka-http-testkit" % "10.1.7",
  "org.scalatest" %% "scalatest" % "3.0.5" % "test",
  "com.wix" %% "accord-core" % "0.7.2",
  "com.pauldijou" %% "jwt-core" % "0.16.0",
  "com.h2database" % "h2" % "1.4.193",
  "com.typesafe.slick" %% "slick" % "3.2.1",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.2.1",
  "io.monix" %% "monix" % "3.0.0-RC2"
)

enablePlugins(PackPlugin)
packMain := Map("rest" -> "challenge.App")